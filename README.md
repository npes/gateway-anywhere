![Build Status](https://gitlab.com/npes/gateway-anywhere/badges/master/pipeline.svg)

# Gateway Anywhere

Building a transportable gateway to utilize in environments where zigbee is needed but access to ethernet is prohibitid or not available.  
This will typically be in industrial environments where IoT systems are to be installed in parrallel to the companys regular network infrastructure.

Link to website: [https://npes.gitlab.io/gateway-anywhere](https://npes.gitlab.io/gateway-anywhere)

# Development instructions

1. create a virtual environment [https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment](https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment)
2. activate the environment [https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#activating-a-virtual-environment](https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#activating-a-virtual-environment)
3. Install pip dependencies `pip install -r requirements.txt`
4. Run site on local dev server `mkdocs serve`
