# Installation of Compute Module 4 PoE Board (B)

- Using the guide at [https://www.waveshare.com/wiki/Compute*Module_4_PoE_Board*(B)](<https://www.waveshare.com/wiki/Compute_Module_4_PoE_Board_(B)>)

1. Installation of operating system [https://www.waveshare.com/wiki/Write_Image_for_Compute_Module_Boards_eMMC_version](https://www.waveshare.com/wiki/Write_Image_for_Compute_Module_Boards_eMMC_version)

   - Problems: Did not work on my dell xps15 with usb 3.0, cm4 drive was not recognized. It did work on my older asus laptop. Reports about incompatible usb chipsets are on the internet: [https://github.com/raspberrypi/usbboot/issues/87](https://github.com/raspberrypi/usbboot/issues/87)
   - Using the 32bit raspberrypi os image with desktop, not recommended programs

2. OS Setup

   - networking (current cm4 is only wired)
   - ssh
   - remote desktop ?
   - remove usb error step 8 here: [https://www.waveshare.com/wiki/Write_Image_for_Compute_Module_Boards_eMMC_version](https://www.waveshare.com/wiki/Write_Image_for_Compute_Module_Boards_eMMC_version)

3. Setup zigbee2mqtt [https://www.zigbee2mqtt.io/](https://www.zigbee2mqtt.io/)
4. Setup raspberry pi zero as zigbee device
5. Test with zigbee devices
